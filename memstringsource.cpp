// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (C) 2020 Anton Kozlov
 * Released under GNU GPL v3, read the file 'LICENSE' for more information.
 */
#include "memstringsource.h"

#include <iostream>

bool MemStringSource::reopen(const IStringSource::String &resource)
{
    //if resource was opened already - close it
    if(this->infile.is_open()) this->Close();
    this->infile.open(resource);

    return this->infile.is_open();
}

bool MemStringSource::init_pool() const
{
    //it's faster than call size() in the cycle
    std::size_t counter(0);

    //fill poll with strings
    do
    {
        String line;
        std::getline(infile,line);
        if(this->infile.eof()) break;
        else this->pool.push(line);
    }
    while(counter++<POOL_SIZE);

    return !this->pool.empty();
}

void MemStringSource::clear_pool()
{
    while(!this->pool.empty()) this->pool.pop();
}

/*!
 * \brief MemStringSource::GetString - gets an string from the pool
 * \param lnumOut - line number
 * \param strOut - value
 */
void MemStringSource::GetString(std::size_t &lnumOut, IStringSource::String &strOut) const
{
    std::lock_guard<std::mutex> lock(this->pool_mutex);

    if(this->pool.empty()&&!this->init_pool())
    {
        //no more lines - send EOF
        lnumOut=LINE_EOF;
        return;
    }

    strOut=this->pool.front();this->pool.pop();

    lnumOut=this->linesCounter++;
}

/*!
 * \brief MemStringSource::Load - opens file and fill the pool with strings
 * \param resource
 * \return
 */
bool MemStringSource::Load(const IStringSource::String &resource)
{
    bool res(false);

    if(this->reopen(resource)&&this->init_pool())
    {
#if DEBUG==1
        std::cout << "The file '" << resource << "' was opened.\n";
#endif
        this->linesCounter=1;
        res=true;
    }
    else
    {
        std::cout << "Can't open file '" << resource << "' or it's empty!\n";
        this->Close();
    }

    return res;
}

void MemStringSource::Close()
{
    //clear pool
    this->clear_pool();
    this->infile.close();

    this->linesCounter=LINE_EOF;
}

#if UNIT_TEST==1
#include "test/doctest.h"

#include <fstream>

TEST_SUITE("MemStringSource tests")
{
    const char* TMP_FILE_PATH="/tmp/input-t1.txt";
    const char* ERROUS_FILE_PATH="/tmpa/asasdsd";
    const int NUM_TEST1_LINES=2500;

    bool create_test_file1(void)
    {
        bool res(false);
        std::ofstream ofile;
        ofile.open(TMP_FILE_PATH);

        if(ofile.is_open())
        {
            for(int i=0;i<NUM_TEST1_LINES;i++)
            {
                ofile << i << std::endl;
            }

            ofile.close();
            res=true;
        }

        return res;
    }

    bool create_test_file2(void)
    {
        bool res(false);
        std::ofstream ofile;
        ofile.open(TMP_FILE_PATH);

        if(ofile.is_open())
        {
            for(int i=0;i<NUM_TEST1_LINES;i++)
            {
                if(i!=0&&i!=(NUM_TEST1_LINES/2)&&i!=(NUM_TEST1_LINES-1)) ofile << i << std::endl;
                else ofile << std::endl;
            }

            ofile.close();
            res=true;
        }

        return res;
    }

    bool create_test_file3(void)
    {
        bool res(false);
        std::ofstream ofile;
        ofile.open(TMP_FILE_PATH);

        if(ofile.is_open())
        {
            ofile.close();
            res=true;
        }

        return res;
    }

    TEST_CASE("Positive1")
    {
        MemStringSource mms;        

        REQUIRE(create_test_file1());
        REQUIRE(mms.Load(std::string(TMP_FILE_PATH)));

        for(int counter=0;;counter++)
        {
            std::size_t num(0);
            MemStringSource::String str;
            mms.GetString(num,str);
            if(num==MemStringSource::LINE_EOF) break;

            CHECK(str==std::to_string(counter));
        }

        mms.Close();
    }

    TEST_CASE("Positive2")
    {
        MemStringSource mms;

        REQUIRE(create_test_file2());
        REQUIRE(mms.Load(std::string(TMP_FILE_PATH)));

        for(int counter=0;;counter++)
        {
            std::size_t num(0);
            MemStringSource::String str;
            mms.GetString(num,str);
            if(num==MemStringSource::LINE_EOF) break;

            if(counter!=0&&counter!=(NUM_TEST1_LINES/2)&&counter!=(NUM_TEST1_LINES-1)) CHECK(str==std::to_string(counter));
            else CHECK(str.empty());
        }

        mms.Close();
    }

    TEST_CASE("Negative")
    {
        MemStringSource mms;

        REQUIRE(create_test_file3());
        CHECK(!mms.Load(std::string(TMP_FILE_PATH)));
        CHECK(!mms.Load(std::string(ERROUS_FILE_PATH)));
    }
}
#endif
