// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (C) 2020 Anton Kozlov
 * Released under GNU GPL v3, read the file 'LICENSE' for more information.
 */
#ifndef SIMPLERESULTSMODEL_H
#define SIMPLERESULTSMODEL_H

#include "iresultsmodel.h"

#include <mutex>
#include <algorithm>

/*!
 * \brief The SimpleResultsModel class - implements the model that stores search results in the ram
 */
class SimpleResultsModel : public IResultsModel
{
private:
    mutable ResultsPool pool;
    std::mutex mutex;
    mutable bool isSorted;

    void sort_pool(void) const
    {
        if(!this->isSorted)
        {
            std::stable_sort(this->pool.begin(),this->pool.end(),
                             [](const Result & lhs, const Result & rhs){return lhs.num < rhs.num;});
            this->isSorted=true;
        }
    }
public:
    SimpleResultsModel():isSorted(false) {}

    void Add(std::size_t lnum, std::size_t lpos,const std::string& s) final
    {
        std::lock_guard<std::mutex> guard(this->mutex);
        pool.push_back(Result(lnum,lpos,s));
        this->isSorted=false;
    }

    std::size_t GetValuesCount(void) const final {sort_pool();return pool.size();}
    ResultsPoolIterator GetIterator(void) const final {sort_pool();return pool.cbegin();}
};

#endif // SIMPLERESULTSMODEL_H
