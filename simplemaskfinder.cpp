// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (C) 2020 Anton Kozlov
 * Released under GNU GPL v3, read the file 'LICENSE' for more information.
 */
#include "simplemaskfinder.h"
#include "regexfinder.h"
#include "iresultsmodel.h"

#include <string>
#include <regex>

/*!
 * \brief SimpleMaskFinder::Find - it's just wrapper to the RegexFinder
 */
std::size_t SimpleMaskFinder::Find(const std::string &in, const std::string &regexp, std::size_t lnum, IResultsModel &model)
{
    std::regex regex(regexp);

    return RegexFinder::Find(in,regex,lnum,model);
}

#if UNIT_TEST==1
#include "test/doctest.h"

#include "simpleresultsmodel.h"
#include "maskvalidator.h"

#include <memory>

TEST_SUITE("SimpleMaskFinder tests")
{
    TEST_CASE("Positive")
    {
        const std::vector<std::string> testStrings {{"And bad mistakes ?"},{"I've made a few."},{"I've had my share of sand kicked in my face"}};
        const std::vector<std::string> refValues {{"bad"},{"mad"},{"had"}};
        const std::vector<std::size_t> refPos {5,6,6};
        const std::string mask(".ad");

        std::unique_ptr<IResultsModel> mdl=std::make_unique<SimpleResultsModel>();
        std::unique_ptr<IMaskFinder> finder=std::make_unique<SimpleMaskFinder>();

        for(auto e:testStrings)
        {
            finder->Find(e,mask,1,*mdl);
        }

        std::size_t num(mdl->GetValuesCount());
        IResultsModel::ResultsPoolIterator it(mdl->GetIterator());

        REQUIRE(num==refValues.size());

        for(std::size_t i=0;i<num;i++,it++)
        {
            CHECK(it->str==refValues[i]);
            CHECK(it->pos==refPos[i]);
            CHECK(it->num==1);
        }
    }

    TEST_CASE("Positive-big-mask")
    {
        const std::string testString
        {"I'am a big mask from the small book. I wanna break your program, but i can't to do it. But i really want to do it!"
         "I'am a big mask from the small book. I wanna break your program, but i can't to do it. But i really want to do it!"
         "I'am a big mask from the small book. I wanna break your program, but i can't to do it. But i really want to do it!"
        };

        const std::vector<std::string> refValues
        {
            {"I'am a big mask from the small book. I wanna break your program, but i can't to do it. But i really"},
            {"I'am a big mask from the small book. I wanna break your program, but i can't to do it. But i really"},
            {"I'am a big mask from the small book. I wanna break your program, but i can't to do it. But i really"}
        };
        const std::vector<std::size_t> refPos {1,115,229};

        const std::string mask {"I'am ? big mask from the small book. I wa?na break your program, but i can't to do it. ?ut i reall?"};
        std::string re;

        REQUIRE(MaskValidator::Validate(mask,re));

        std::unique_ptr<IResultsModel> mdl=std::make_unique<SimpleResultsModel>();
        std::unique_ptr<IMaskFinder> finder=std::make_unique<SimpleMaskFinder>();

        finder->Find(testString,re,2,*mdl);

        std::size_t num(mdl->GetValuesCount());
        IResultsModel::ResultsPoolIterator it(mdl->GetIterator());

        REQUIRE(num==refValues.size());

        for(std::size_t i=0;i<num;i++,it++)
        {
            CHECK(it->str==refValues[i]);
            CHECK(it->pos==refPos[i]);
            CHECK(it->num==2);
        }
    }
}
#endif


