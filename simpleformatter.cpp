// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (C) 2020 Anton Kozlov
 * Released under GNU GPL v3, read the file 'LICENSE' for more information.
 */
#include "simpleformatter.h"

/*!
 * \brief SimpleFormatter::MakeOutput - makes output from the model
 */
void SimpleFormatter::MakeOutput(const IResultsModel &data)
{
    std::size_t numItems(data.GetValuesCount());

    this->out << numItems << std::endl;

    IResultsModel::ResultsPoolIterator it(data.GetIterator());

    for(std::size_t n=0;n<numItems;n++,it++)
    {
        this->out << it->num << " " << it->pos << " " << it->str << std::endl;
    }
}

#if UNIT_TEST==1
#include "test/doctest.h"

#include "simpleresultsmodel.h"

#include <sstream>

TEST_SUITE("SimpleFormatter tests")
{
    const int NUM_TEST_LINES=10;
    const int TEST_LPOS=2;

    void create_test_model1(IResultsModel& m,std::ostringstream& ref1)
    {
        ref1 << NUM_TEST_LINES << std::endl;

        for(std::size_t i=0;i<NUM_TEST_LINES;i++)
        {
            //update model
            m.Add(i,TEST_LPOS,std::to_string(i));
            //upd reference
            ref1 << i << " " << TEST_LPOS << " " << i << std::endl;
        }
    }

    //simulate mt find
    void create_test_model2(IResultsModel& m,std::ostringstream& ref1)
    {
        ref1 << NUM_TEST_LINES << std::endl;

        for(std::size_t i=0;i<NUM_TEST_LINES;i+=2)
        {
            //update model
            m.Add(i+1,TEST_LPOS,std::to_string(i+1));
            m.Add(i,TEST_LPOS,std::to_string(i));
            //upd reference
            ref1 << i << " " << TEST_LPOS << " " << i << std::endl;
            ref1 << (i+1) << " " << TEST_LPOS << " " << (i+1) << std::endl;
        }
    }

    TEST_CASE("Positive")
    {
        SimpleResultsModel mdl;
        std::ostringstream fmt_buf,reference;
        create_test_model1(static_cast<IResultsModel&>(mdl),reference);

        SimpleFormatter formatter(static_cast<std::ostream&>(fmt_buf));
        formatter.MakeOutput(mdl);

        CHECK(reference.str()==fmt_buf.str());
    }

    TEST_CASE("Positive-mt")
    {
        SimpleResultsModel mdl;
        std::ostringstream fmt_buf,reference;
        create_test_model2(static_cast<IResultsModel&>(mdl),reference);

        SimpleFormatter formatter(static_cast<std::ostream&>(fmt_buf));
        formatter.MakeOutput(mdl);

        CHECK(reference.str()==fmt_buf.str());
    }
}
#endif
