// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (C) 2020 Anton Kozlov
 * Released under GNU GPL v3, read the file 'LICENSE' for more information.
 */
#ifndef MEMSTRINGSOURCE_H
#define MEMSTRINGSOURCE_H

#include "istringsource.h"

#include <queue>
#include <fstream>
#include <mutex>

/*!
 * \brief The MemStringSource class - it's simplest implementation of the string source (stores strings in the pool in ram)
 */
class MemStringSource : public IStringSource
{
private:
#if UNIT_TEST==1
    const std::size_t POOL_SIZE=1024;
#else
    const std::size_t POOL_SIZE=10000;
#endif

    using StringPool=std::queue<String>;

    mutable StringPool pool;
    mutable std::ifstream infile;
    mutable std::size_t linesCounter;///<lines counter
    mutable std::mutex pool_mutex;

    bool reopen(const String& resource);
    bool init_pool(void) const;
    void clear_pool(void);
public:

    explicit MemStringSource():linesCounter(LINE_EOF) {}
    ~MemStringSource() {this->Close();}

    void GetString(std::size_t& lnumOut,String& strOut) const final;
    bool Load(const String& resource) final;
    void Close(void) final;
};

#endif // MEMSTRINGSOURCE_H
