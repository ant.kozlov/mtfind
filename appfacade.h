// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (C) 2020 Anton Kozlov
 * Released under GNU GPL v3, read the file 'LICENSE' for more information.
 */

#ifndef APPFACADE_H
#define APPFACADE_H

#include <iostream>
#include <string>
#include <memory>

#include "istringsource.h"
#include "imaskfinder.h"
#include "iresultsmodel.h"
#include "ioutputformatter.h"

/*!
 * \brief The AppFacade class contains all business logic of the app
 */
class AppFacade
{
private:
    const std::size_t DEF_NUM_THREADS=4;

    using IStringSourcePtr=std::unique_ptr<IStringSource>;
    using IMaskFinderPtr=std::unique_ptr<IMaskFinder>;
    using IResultsModelPtr=std::unique_ptr<IResultsModel>;
    using IOutputFormatterPtr=std::unique_ptr<IOutputFormatter>;

    std::string fileName,mask;
    bool isInitialized;

    IStringSourcePtr stringSource;
    IMaskFinderPtr maskFinder;
    IResultsModelPtr model;
    IOutputFormatterPtr formatter;

    AppFacade():isInitialized(false) {}

    bool setFilename(const std::string& fn);
    bool setMask(const std::string& m);

    void createStringSource(void);
    void createMaskFinder(void);
    void createModelAndFormatter(std::ostream& output);
    int getHWThreadNumber(void) const;
#if DEBUG==1
    void printStats(std::size_t stat[],int num) const;
#endif
public:
    static AppFacade& Instance()
    {
        static AppFacade instance;
        return instance;
    }

    bool Init(const std::string& fname,const std::string& m,std::ostream& output=std::cout);
    void Process(void);
    void Finish(void);

    AppFacade(const AppFacade&) = delete;
    AppFacade& operator=(const AppFacade&)  = delete;
};

#endif // APPFACADE_H
