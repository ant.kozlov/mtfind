// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (C) 2020 Anton Kozlov
 * Released under GNU GPL v3, read the file 'LICENSE' for more information.
 */
#ifndef IRESULTSMODEL_H
#define IRESULTSMODEL_H

#include <string>
#include <vector>

/*!
 * \brief itинтерфейс модели, хранящей результаты поиска
 */

/*!
 * \brief The IResultsModel class - it's the interface to the model that stores results of the find process
 */
class IResultsModel
{
private:
    IResultsModel(const IResultsModel&)=delete;
    IResultsModel& operator = (const IResultsModel&)=delete;
public:
    explicit IResultsModel() {}
    virtual ~IResultsModel() {}

    struct Result
    {
        std::size_t num, pos;
        std::string str;

        explicit Result():num(0),pos(0) {}
        explicit Result(std::size_t lnum, std::size_t lpos,const std::string& s):
            num(lnum),pos(lpos),str(s) {}
    };    

    using ResultsPool=std::vector<Result>;
    using ResultsPoolIterator=ResultsPool::const_iterator;

    virtual void Add(std::size_t lnum, std::size_t lpos,const std::string& s)=0;///<it's must be thread-safe
    virtual std::size_t GetValuesCount(void) const = 0;    
    virtual ResultsPoolIterator GetIterator(void) const =0;
};

#endif // IRESULTSMODEL_H
