// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (C) 2020 Anton Kozlov
 * Released under GNU GPL v3, read the file 'LICENSE' for more information.
 */
#ifndef SIMPLEFORMATTER_H
#define SIMPLEFORMATTER_H

#include "ioutputformatter.h"

#include <iostream>

/*!
 * \brief The SimpleFormatter class - implements output generation from the model
 */
class SimpleFormatter : public IOutputFormatter
{
private:
    std::ostream& out;
public:
    explicit SimpleFormatter(void):out(std::cout) {}
    explicit SimpleFormatter(std::ostream& o):out(o) {}

    void MakeOutput(const IResultsModel& data);
};

#endif // SIMPLEFORMATTER_H
