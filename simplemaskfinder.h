// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (C) 2020 Anton Kozlov
 * Released under GNU GPL v3, read the file 'LICENSE' for more information.
 */
#ifndef SIMPLEMASKFINDER_H
#define SIMPLEMASKFINDER_H

#include "imaskfinder.h"

#include <string>

/*!
 * \brief The SimpleMaskFinder class - it's just wrapper to the RegexFinder
 */
class SimpleMaskFinder : public IMaskFinder
{
private:
public:
    SimpleMaskFinder() {}

    std::size_t Find(const std::string& in,
                             const std::string &regexp,
                             std::size_t lnum,
                             IResultsModel &model) final;
};

#endif // SIMPLEMASKFINDER_H
