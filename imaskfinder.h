// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (C) 2020 Anton Kozlov
 * Released under GNU GPL v3, read the file 'LICENSE' for more information.
 */
#ifndef IMASKFINDER_H
#define IMASKFINDER_H

#include <string>

class IResultsModel;

/*!
 * \brief The IMaskFinder class - it's the interface to the mask finder implementation
 */
class IMaskFinder
{
private:
    IMaskFinder(const IMaskFinder&)=delete;
    IMaskFinder& operator = (const IMaskFinder&)=delete;
public:
    explicit IMaskFinder() {}
    virtual ~IMaskFinder() {}

    virtual std::size_t Find(const std::string& in,
                             const std::string &regexp,
                             std::size_t lnum,
                             IResultsModel &model)=0;
};

#endif // IMASKFINDER_H
