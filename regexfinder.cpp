// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (C) 2020 Anton Kozlov
 * Released under GNU GPL v3, read the file 'LICENSE' for more information.
 */
#include "regexfinder.h"

#if DEBUG==1
#include <iostream>
#endif

/*!
 * \brief RegexFinder::Find - finds matches and stores it to the model
 */
std::size_t RegexFinder::Find(const std::string &in, const std::regex &re, std::size_t lnum, IResultsModel &model)
{
    auto words_begin=std::sregex_iterator(in.begin(), in.end(), re);
    auto words_end = std::sregex_iterator();
    auto num(std::distance(words_begin, words_end));

    if(num>0)
    {
        for (std::sregex_iterator i = words_begin; i != words_end; ++i)
        {
            std::smatch match = *i;
            model.Add(lnum,(match.position()+1),match.str());
        }
    }

    return (std::size_t)num;
}
