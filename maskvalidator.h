// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (C) 2020 Anton Kozlov
 * Released under GNU GPL v3, read the file 'LICENSE' for more information.
 */
#ifndef MASKVALIDATOR_H
#define MASKVALIDATOR_H

#include <string>

/*!
 * \brief The MaskValidator class - validates the mask
 */
class MaskValidator
{
private:    
    constexpr static const char* MASK_FILTER="[A-Za-z0-9,.:;?' ]";

    explicit MaskValidator()=delete;
    MaskValidator(const MaskValidator&)=delete;
    MaskValidator& operator =(const MaskValidator&)=delete;
public:
    static bool Validate(const std::string& in, std::string& out);

    static const std::size_t MASK_MAX_LEN=100;
};

#endif // MASKVALIDATOR_H
