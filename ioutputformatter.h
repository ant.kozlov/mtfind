// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (C) 2020 Anton Kozlov
 * Released under GNU GPL v3, read the file 'LICENSE' for more information.
 */
#ifndef IOUTPUTFORMATTER_H
#define IOUTPUTFORMATTER_H

#include "iresultsmodel.h"

/*!
 * \brief The IOutputFormatter class - it's the interface to the output formatter implementaion
 */
class IOutputFormatter
{
private:
    IOutputFormatter(const IOutputFormatter&)=delete;
    IOutputFormatter& operator = (const IOutputFormatter&)=delete;
public:
    explicit IOutputFormatter() {}
    virtual ~IOutputFormatter() {}

    virtual void MakeOutput(const IResultsModel& data)=0;
};

#endif // IOUTPUTFORMATTER_H
