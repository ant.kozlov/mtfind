// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (C) 2020 Anton Kozlov
 * Released under GNU GPL v3, read the file 'LICENSE' for more information.
 */
#include "appfacade.h"
#include "memstringsource.h"
#include "simplemaskfinder.h"
#include "simpleresultsmodel.h"
#include "simpleformatter.h"
#include "maskvalidator.h"

#include <iostream>
#include <thread>
#include <vector>

/*!
 * \brief AppFacade::Init - it checks input params for validity, create needed objects and open input file
 * \param fname - the filename
 * \param m - the mask
 * \param output - output stream
 * \return true if ok
 */
bool AppFacade::Init(const std::string &fname, const std::string &m, std::ostream &output)
{
    bool res(false);

    if(!this->isInitialized)
    {
        this->fileName.clear();
        this->mask.clear();

        if(!this->setFilename(fname)) output << "Bad filename!\n";
        else if(!this->setMask(m)) output << "Bad mask!\n";
        else
        {
            this->createStringSource();            

            res=this->isInitialized=this->stringSource->Load(fileName);

            if(res)
            {
                this->createMaskFinder();
                this->createModelAndFormatter(output);
            }
        }
    }

    return res;
}

/*!
 * \brief threadFunc - this function gets a string from the source and implements find process
 */
static void threadFunc(const IStringSource& src,IMaskFinder& finder,const std::string& mask,
                       IResultsModel& model,
                       std::size_t* processed)
{
    std::size_t lineNum(0);
    std::string curStr;
    *processed=0;

    while(1)
    {
        src.GetString(lineNum,curStr);
        if(lineNum==IStringSource::LINE_EOF) break;
        finder.Find(curStr,mask,lineNum,model);
#if DEBUG==1
        (*processed)++;
#endif
    }
}

/*!
 * \brief AppFacade::Process - implements MT execution of the threadFunc
 */
void AppFacade::Process()
{    
    if(this->isInitialized)
    {
        const int NUM_THREADS(this->getHWThreadNumber());
        std::size_t proc[NUM_THREADS];///<statistics

        std::vector<std::thread> threadPool;
        threadPool.reserve(NUM_THREADS);

        for(int n=0;n<NUM_THREADS;n++)
        {
            threadPool.emplace_back(threadFunc,
                                    std::ref(*this->stringSource),
                                    std::ref(*this->maskFinder),
                                    std::ref(this->mask),
                                    std::ref(*this->model),
                                    &proc[n]);
        }

        for(std::thread& e:threadPool) e.join();
        threadPool.clear();

        this->formatter->MakeOutput(*this->model);

#if DEBUG==1
        this->printStats(proc,NUM_THREADS);
#endif
    }
}

/*!
 * \brief AppFacade::Finish - frees resources
 */
void AppFacade::Finish()
{    
    if(this->isInitialized)
    {
        this->stringSource->Close();
        this->stringSource.reset(nullptr);
        this->maskFinder.reset(nullptr);
        this->model.reset(nullptr);
        this->formatter.reset(nullptr);
        this->isInitialized=false;
    }
}

bool AppFacade::setFilename(const std::string &fn)
{
    //FIXME add more strong check if needed
    bool res(!fn.empty());
    if(res) this->fileName=fn;
    return res;
}

bool AppFacade::setMask(const std::string &m)
{
    return MaskValidator::Validate(m,this->mask);
}

void AppFacade::createStringSource()
{
    this->stringSource=std::make_unique<MemStringSource>();
}

void AppFacade::createMaskFinder()
{
    this->maskFinder=std::make_unique<SimpleMaskFinder>();
}

void AppFacade::createModelAndFormatter(std::ostream& output)
{
    this->model=std::make_unique<SimpleResultsModel>();
    this->formatter=std::make_unique<SimpleFormatter>(output);
}

int AppFacade::getHWThreadNumber() const
{
#if USE_MT==1
    int res(std::thread::hardware_concurrency());
    return (res==0 ? DEF_NUM_THREADS : res);
#else
    return 1;
#endif
}

#if DEBUG==1
void AppFacade::printStats(std::size_t stat[], int num) const
{
    std::size_t sum(0);
    std::cout << "Stats: ";

    for(int i=0;i<num;i++) {sum+=stat[i];std::cout << stat[i] << ", ";}

    std::cout << "sum: " << sum << std::endl;
}
#endif

#if UNIT_TEST==1
#include "test/doctest.h"

#include "simpleresultsmodel.h"

#include <sstream>
#include <fstream>

TEST_SUITE("AppFacade tests")
{
    bool make_test_file(const std::string& path)
    {
        const std::string text("I've paid my dues\n"
                               "Time after time.\n"
                               "I've done my sentence\n"
                               "But committed no crime.\n"
                               "And bad mistakes ?\n"
                               "I've made a few.\n"
                               "I've had my share of sand kicked in my face\n"
                               "But I've come through.\n");

        std::ofstream ofile;
        bool res(false);

        ofile.open(path);

        if(ofile.is_open())
        {
            ofile << text;
            ofile.close();
            res=true;
        }

        return res;
    }

    void make_ref_output(std::ostream& out)
    {
        out << "3\n" << "5 5 bad\n" << "6 6 mad\n" << "7 6 had\n";
    }

    /*
    ПРИМЕР
    Файл input.txt:
    1  I've paid my dues
    2  Time after time.
    3  I've done my sentence
    4  But committed no crime.
    5  And bad mistakes ?
    6  I've made a few.
    7  I've had my share of sand kicked in my face
    8  But I've come through.

    Запуск программы: mtfind input.txt "?ad"
    Ожидаемый результат:
      3
      5 5 bad
      6 6 mad
      7 6 had
    */
    TEST_CASE("Positive")
    {
        const std::string file_path {"/tmp/input-mtf1.txt"};
        const std::string mask {"\"?ad\""};
        std::ostringstream out_capture,reference;

        REQUIRE(make_test_file(file_path));
        make_ref_output(reference);

        REQUIRE(AppFacade::Instance().Init(file_path,mask,out_capture));
        AppFacade::Instance().Process();

        CHECK(out_capture.str()==reference.str());

        AppFacade::Instance().Finish();
    }

    TEST_CASE("Negative-bad_file")
    {
        const std::string file_path1 {"/tmp/7jhfddhainis8input-mtf1.txt"};
        const std::string file_path2 {""};
        const std::string mask {"\"?ad\""};
        std::ostringstream out_capture;

        CHECK(!AppFacade::Instance().Init(file_path1,mask,out_capture));
        AppFacade::Instance().Finish();
        CHECK(!AppFacade::Instance().Init(file_path2,mask,out_capture));
        AppFacade::Instance().Finish();
    }

    TEST_CASE("Negative-bad_mask")
    {
        const std::string file_path {"/tmp/7jhfddhainis8input-mtf1.txt"};
        const std::string mask {"\"\""};
        std::ostringstream out_capture;

        CHECK(!AppFacade::Instance().Init(file_path,mask,out_capture));
        AppFacade::Instance().Finish();
    }
}
#endif
