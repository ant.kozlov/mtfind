// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (C) 2020 Anton Kozlov
 * Released under GNU GPL v3, read the file 'LICENSE' for more information.
 */
#include "maskvalidator.h"

#include <regex>

#if DEBUG==1
#include <iostream>
#endif

/*!
 * \brief MaskValidator::Validate - validates the mask and converts it to regexp
 */
bool MaskValidator::Validate(const std::string &in, std::string &out)
{
    bool res(!in.empty()&&in.size()<=MASK_MAX_LEN);

    if(res)
    {
        const std::string valid_re_mask(MASK_FILTER);
        std::regex vre(valid_re_mask);

        auto words_begin=std::sregex_iterator(in.begin(), in.end(), vre);
        auto words_end = std::sregex_iterator();

        auto num(std::distance(words_begin, words_end));

        if(num>0)
        {
            out.clear();

            for(std::sregex_iterator i = words_begin; i != words_end; ++i)
            {
                std::smatch match = *i;
                std::string match_str = match.str();

                if(match_str=="?") match_str=".";
                else if(match_str==".") match_str="\\.";
                else if(match_str==",") match_str="\\,";
                else if(match_str==";") match_str="\\;";
                else if(match_str==":") match_str="\\:";
                else if(match_str=="'") match_str="\\'";

                out+=match_str;
            }

#if DEBUG==1
            std::cout << " mask_in: " << in << std::endl;
            std::cout << "mask_out: " << out << std::endl;
#endif

            res=!out.empty();
        }
        else res=false;
    }

    return res;
}

#if UNIT_TEST==1
#include "test/doctest.h"

TEST_SUITE("MaskValidator tests")
{
    TEST_CASE("Positive")
    {
        const std::string in("mask? tEst.,;: 1@#$ Re?'");
        const std::string result("mask. tEst\\.\\,\\;\\: 1 Re.\\'");
        std::string out;

        bool res=MaskValidator::Validate(in,out);

        CAPTURE(in);
        CAPTURE(result);
        CAPTURE(out);

        CHECK(res);
        CHECK(result==out);
    }

    TEST_CASE("Negative")
    {
        const std::string in("~!@#$%^&*()_-+=]}[{\\|/><\n\r");
        const std::string result("");
        std::string out;

        bool res=MaskValidator::Validate(in,out);

        CAPTURE(in);
        CAPTURE(result);
        CAPTURE(out);

        CHECK(res==false);
        CHECK(result==out);
    }

    TEST_CASE("Max len")
    {
        const std::string in("a",MaskValidator::MASK_MAX_LEN+1);
        const std::string result("");
        std::string out;

        bool res=MaskValidator::Validate(in,out);

        CHECK(res==false);
        CHECK(result==out);
    }

    TEST_CASE("Empty")
    {
        const std::string in("");
        const std::string result("");
        std::string out;

        bool res=MaskValidator::Validate(in,out);

        CHECK(res==false);
        CHECK(result==out);
    }
}

#endif
