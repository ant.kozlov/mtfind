// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (C) 2020 Anton Kozlov
 * Released under GNU GPL v3, read the file 'LICENSE' for more information.
 */
#ifndef REGEXFINDER_H
#define REGEXFINDER_H

#include <string>
#include <regex>

#include "iresultsmodel.h"

/*!
 * \brief The RegexFinder class - implements mask finding (uses std::regex for it)
 */
class RegexFinder
{
private:
    explicit RegexFinder()=delete;
    RegexFinder(const RegexFinder&)=delete;
    RegexFinder& operator =(const RegexFinder&)=delete;
public:
    static std::size_t Find(const std::string& in, const std::regex &re,
                            std::size_t lnum, IResultsModel &model);
};

#endif // REGEXFINDER_H
