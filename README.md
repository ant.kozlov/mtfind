# mtfind

The 'mtfind' is an command line utility that doing mask search in the input text file by using multi thread processing.

## Usage

```
$ mtfind input-file "mask"
```

The mask length must be less or equal of 100 and it must match the following regexp MASK_FILTER="[A-Za-z0-9,.:;?' ]".
The '?' symbol in the mask denotes the substitution of any character.

The output has the following format:
```
overall hit counter
"the line number" "hit position in the line" "hit value"
```

## Installation
To build the mtfind do this:
1. Make sure that you have GCC and CMake installed.
2. Run build.sh script.
3. After the build you will find binary of mtfind in the 'build' directory.

