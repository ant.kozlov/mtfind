#!/bin/bash

echo "Prepare for mtfind build..."

rm -R ./build

mkdir ./build

cd ./build

cmake ../

make

